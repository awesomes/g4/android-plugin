// This is a generated file. Not intended for manual editing.
package com.android.tools.idea.compose.preview.util.device.parser;

import java.util.List;
import org.jetbrains.annotations.*;
import com.intellij.psi.PsiElement;

public interface DeviceSpecSpec extends PsiElement {

  @NotNull
  List<DeviceSpecParam> getParamList();

}
